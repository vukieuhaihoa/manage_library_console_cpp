#include "Menu_Manager.h"
#include <iomanip>

using namespace std;

extern map<string, Infor_Account> List_Infor_Account;
extern Infor_Account Account_Curren;
extern map<string, Infor_Book> List_Infor_Books;
extern Infor_User_Borrowed List_Book_Borrowed[100];
//extern Infor_Book Book_Selected;


void Menu_Manager()
{
	string temp = "";
	int choose = 0;
	while (true)
	{
		system("cls");
		Load_form_Manager();
		cout << "\nCac chuc nang cho quan ly he thong:";
		cout << "\n1.Truy cap kho sach cua thu vien";
		cout << "\n2.Duyet cho tai khoan muon sach"; // Coding
		cout << "\n3.Dang thong bao"; // Done
		cout << "\n4.Xem thong tin tai khoan"; // Done
		cout << "\n5.Cap nhat thong tin tai khoan"; // Done
		cout << "\n6.Thay doi mat khau"; // Done
		cout << "\n7.Thoat ung dung"; // Done
		cout << "\nMoi ban chon tinh nang:";
		cin >> choose;
		cin.ignore();
		if (choose == 1)
		{
			while (true)
			{
				system("cls");
				Load_form_ListBook();
				Set_Color(7);
				cout << "Moi ban chon cac tinh nang sau:\n";
				cout << "1.Tai toan bo danh sach cac sach co trong thu vien\n"; //Done
				cout << "2.Tim sach theo ten\n"; //Done
				cout << "3.Them sach moi\n"; //Done
				cout << "4.Cap nhat kho sach\n";
				cout << "5.Xoa mot sach ra khoi kho\n"; //Done
				cout << "6.Tro ve menu ban dau\n"; //Done
				cout << "Moi ban nhap:";
				int choo = 0;
				cin >> choo;
				cin.ignore();
				if (choo == 1)
				{
					system("cls");
					Print_List_Book();
					string tempp = "";
					cout << "ENTER de quay lai Menu truoc!";
					getline(cin, tempp);
				}
				else if (choo == 2)
				{
					system("cls");
					cout << "Searching...\n";
					cout << "Moi ban nhap vao ten hoac mot phan ten cua sach can tim:";
					string Name_of_Book = "";
					getline(cin, Name_of_Book);
					bool check_exist = Search_Book_Name(Name_of_Book);
					if (check_exist)
					{
						string tempp = "";
						cout << "ENTER de quay lai Menu truoc!";
						getline(cin, tempp);
					}
					else
					{
						cout << "Noi dung khong tim thay :(\n";
						string tempp = "";
						cout << "ENTER de quay lai Menu truoc!";
						getline(cin, tempp);
					}
				}
				else if (choo == 3)
				{
					Infor_Book _tempnewbook;
					system("cls");
					/*code o day*/
					cout << "Moi ban nhap vao thong tin cua sach moi: \n";
					cout << "Id: ";
					getline(cin, _tempnewbook.Id);
					// Bo sung ham check xem id da ton tai chua
					/*
					
					
					*/
					cout << "Ten: ";
					getline(cin, _tempnewbook.Name);
					// Bo sung ham check xem ten da ton tai chua
					/*
					
					
					*/
					cout << "So luong: ";
					cin >> _tempnewbook.Amount;
					cin.ignore();
					cout << "Gia tien: ";
					cin >> _tempnewbook.Price;
					cin.ignore();
					cout << "Ten tac gia: ";
					getline(cin, _tempnewbook.Author);
					cout << "Mo ta: ";
					getline(cin, _tempnewbook.Describe);
					bool check_add_book = Add_Book(_tempnewbook);
					if (check_add_book)
					{
						system("cls");
						cout << "Them sach moi thanh cong! Enter de quay lai menu truoc";
						getline(cin, temp);
					}
					else
					{
						system("cls");
						cout << "Them sach moi that bai! Enter de quay lai menu truoc";
						getline(cin, temp);
					}
				}
				else if (choo == 4)
				{
					string Id = "";
					system("cls");
					cout << "Cap nhat thong tin sach theo Id: \n";
					cout << "Moi ban nhap vao id cua sach can cap nhat: ";
					getline(cin, Id);
					int _result = Update_Book(Id);
					if (_result == 0)
					{
						system("cls");
						cout << "Id khong ton tai trong DB, xin kiem tra lai! Enter de quay lai menu truoc\n";
						getline(cin, temp);
					}
					else
					{
						system("cls");
						cout << "cap nhat thanh cong! Enter de quay lai menu truoc\n";
						getline(cin, temp);
					}

				}
				else if (choo == 5)
				{
					string Id = "";
					system("cls");
					cout << "Xoa mot sach ra khoi kho: \n";
					cout << "Moi ban nhap vao id cua sach can xoa: ";
					getline(cin, Id);
					int result = Delete_Book(Id);
					if (result == 0)
					{
						system("cls");
						cout << "Id khong ton tai trong DB, xin kiem tra lai! Enter de quay lai menu truoc\n";
						getline(cin, temp);
					}
					else if (result == 1)
					{
						system("cls");
						cout << "Id da co nguoi muon khong duoc xoa, xin kiem tra lai! Enter de quay lai menu truoc\n";
						getline(cin, temp);
					}
					else
					{
						system("cls");
						cout << "Xoa thanh cong! Enter de quay lai menu truoc\n";
						getline(cin, temp);
					}

				}
				else if (choo == 6)
				{
					break;
				}
			}
			
		}
		else if (choose == 2)
		{
			system("cls");
			string Id_Book = "";
			string Name_Book = "";
			int Amount = 0;
			string Id_User = "";
			cout << "Duyet cho sinh vien muon sach:\n";
			cout << "Moi ban nhap vao id sach:";
			getline(cin, Id_Book);
			cout << "Moi ban nhap vao id user:";
			getline(cin, Id_User);
			bool Check_Book_Exist = Check_Id_Book_Exist(Id_Book, Name_Book, Amount);
			bool Check_User_Exists = Check_User_Exist(Id_User);
			if (Check_Book_Exist && Check_User_Exists)
			{
				if (Amount <= 0)
				{
					cout << "Id:" << Id_Book << " da cho muon het, vui long cho den khi doc gia tra lai!\n";
					cout << "ENTER de quay lai Menu truoc!";
					getline(cin, temp);
				}
				else
				{
					string Note = "";
					Date Today, Outdated_Day;
					GetTime_System(Today);
					Outdated_Day = GetTime_After_x(Today, 10);
					bool check = Accept_Borrow_Book(Id_Book, Name_Book, Id_User, Today, Outdated_Day, Note);
					//Giam so sach cua id_book hien co trong thu vien xuong 1
					Infor_Book it = List_Infor_Books.find(Name_Book)->second;
					it.Amount--;
					List_Infor_Books[Name_Book] = it;
					check = Save_DB_Book();
					cout << "Duyet thanh cong!!!\n";
					cout << "ENTER de quay lai Menu truoc!";
					getline(cin, temp);
				}
			}
			else
			{
				if (!Check_Book_Exist)
				{
					cout << "Id:" << Id_Book << " khong ton tai trong kho sach thu vien, xin tra lai ma cho dung!\n";
				}
				if (!Check_User_Exists)
				{
					cout << "Id user:" << Id_User << " khong ton tai!\n";
				}
				cout << "ENTER de quay lai Menu truoc!";
				getline(cin, temp);
			}
		}
		else if (choose == 3)
		{
			string content = "";
			Date Time_Post;
			system("cls");
			cout << "POST THONG BAO DEN CAC DOC GIA\n";
			cout << "Nhap vao noi dung thong bao:";
			getline(cin, content);
			GetTime_System(Time_Post);
			bool check = Post_Announce(content, Time_Post);
			cout << "Thong bao da gui den tat ca doc gia.^^\n";
			cout << "ENTER de quay lai Menu truoc!";
			getline(cin, temp);
		}
		else if (choose == 4)
		{
			Show_Infor();
		}
		else if (choose == 5)
		{
			bool check_update = false;
			check_update = Update_Account();
			system("cls");
			if (check_update)
			{
				cout << "Cap nhat thong tin tai khoan thanh cong\n";
				cout << "ENTER de quay lai man hinh chinh\n";
				getline(cin, temp);
			}
			else
			{
				cout << "Cap nhat thong tin tai khoan that bai\n";
				return;
			}
		}
		else if (choose == 6)
		{
			bool check = Change_PassWord();
			system("cls");
			if (check)
			{
				cout << "CAP NHAT MAT KHAU THANH CONG\n";
				cout << "Enter de quay lai man hinh chinh\n";
				getline(cin, temp);

			}
			else
			{
				cout << "CAP NHAT MAT KHAU THAT BAI\n";
				return;
			}
		}
		else if (choose == 7)
		{
			return;
		}
	}
}

void Load_form_Manager()
{
	string temp = "";
	Set_Color(12);
	ifstream Load_file("DataBase/LogoMenuManager.txt", ios::in);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}
	while (!Load_file.eof())
	{
		temp = "";
		getline(Load_file, temp);
		cout << "\n" << temp;
	}
	Set_Color(7);
	Load_file.close();
}

bool Search_Book_Name(string keyword)
{
	map<string, Infor_Book> result;
	bool check = false;
	int position_first = -1;
	for (auto& x : List_Infor_Books)
	{
		position_first = x.first.find(keyword);
		if (position_first != -1)
		{
			check = true;
			result[x.first] = x.second;
		}
	}
	if (check == true)
	{
		cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		cout << "|   ID   |            Name            | Price | AMT |      Author      |                   Describes                 |\n";
		cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		cout << fixed << setfill(' ');
		for (auto& x : result)
		{
			cout << "|" << setw(8) << x.second.Id << "|" << setw(28) << x.second.Name << "|" << setw(7) << setfill(' ') << x.second.Price << "|" << setw(5) << x.second.Amount << "|" << setw(18) << x.second.Author << "|" << setw(45) << x.second.Describe << "|\n";
			cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		}
	}
	return check;
}

bool Check_Id_Book_Exist(string IdBook, string &NameBook, int &Amount)
{
	//map<string, Infor_Book>::iterator it;
	for (auto &i : List_Infor_Books)
	{
		if (IdBook.compare(i.second.Id) == 0)
		{
			NameBook = i.second.Name;
			Amount = i.second.Amount;
			return true;
		}
	}
	return false;
}

bool Accept_Borrow_Book(string Id_Book, string NameBook,string Id_User, Date Today, Date Outdated_day, string Note) // Ghi nhan thong tin can muon sach dua va danh sach can duyet de duoc muon sach
{
	string temp = "";
	//Id sinh vien|Id sach|Ten sach|
	string temp_today_day = "";
	string temp_today_mon = "";
	if (Today.Day < 10)
	{
		temp_today_day = "0" + to_string(Today.Day);
	}
	else
		temp_today_day = to_string(Today.Day);

	if (Today.Month < 10)
	{
		temp_today_mon = "0" + to_string(Today.Month);
	}
	else
		temp_today_mon = to_string(Today.Month);


	string temp_outdated_day = "";
	string temp_outdated_mon = "";

	if (Outdated_day.Day < 10)
	{
		temp_outdated_day = "0" + to_string(Outdated_day.Day);
	}
	else
		temp_outdated_day = to_string(Outdated_day.Day);

	if (Outdated_day.Month < 10)
	{
		temp_outdated_mon = "0" + to_string(Outdated_day.Month);
	}
	else
		temp_outdated_mon = to_string(Outdated_day.Month);

	temp = Id_User + "|" + Id_Book + "|" + NameBook + "|"+ temp_today_day+ "/" + temp_today_mon + "/" + to_string(Today.Year) + "|" + temp_outdated_day + "/" + temp_outdated_mon + "/" + to_string(Outdated_day.Year)+ "|" + Note + "|\n";
	//Luu xuong danh sach Borrow_Book.txt
	Store_DB("DataBase/Borrowed_Book.txt", temp);
	return true;
}

bool Check_User_Exist(string Id)
{
	//map<string, Infor_Book>::iterator it;
	for (auto &i : List_Infor_Account)
	{
		if (Id.compare(i.second.Id) == 0)
		{
			return true;
		}
	}
	return false;
}

bool Save_DB_Book()
{
	string temp = "";
	ofstream Load_file("DataBase/Books.txt", ios::trunc);
	Load_file.close();
	Load_file.open("DataBase/Books.txt", ios::out | ios::app);
	remove("DataBase/Books.txt");
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		return 0;
	}
	bool check = false;
	for (auto &x : List_Infor_Books)
	{
		temp = "";
		if (check == false)
		{
			temp = x.second.Id + "|" + x.second.Name + "|" +  to_string(x.second.Amount) + "|" + to_string(x.second.Price) + "|" + x.second.Author + "|" + x.second.Describe + "|";
			check = true;
		}
		else
			temp = "\n" + x.second.Id + "|" + x.second.Name + "|" + to_string(x.second.Amount) + "|" + to_string(x.second.Price) + "|" + x.second.Author + "|" + x.second.Describe + "|";
		Load_file << temp;
	}
	Load_file.close();
	return 1;
}

bool Post_Announce(string content, Date Time_Post)
{
	string temp = "";
	string temp_today_day = "";
	string temp_today_mon = "";
	if (Time_Post.Day < 10)
	{
		temp_today_day = "0" + to_string(Time_Post.Day);
	}
	else
		temp_today_day = to_string(Time_Post.Day);

	if (Time_Post.Month < 10)
	{
		temp_today_mon = "0" + to_string(Time_Post.Month);
	}
	else
		temp_today_mon = to_string(Time_Post.Month);

	temp = Account_Curren.FullName + "|" + temp_today_day + "/" + temp_today_mon + "/" + to_string(Time_Post.Year) + "|" + content + "|\n";
	Store_DB("DataBase/Announce.txt", temp);
	return true;
}

bool Add_Book(Infor_Book _inforbook)
{
	string _temp = "";
	List_Infor_Books[_inforbook.Name] = _inforbook;
	_temp = "\n" + _inforbook.Id + "|" + _inforbook.Name + "|" + to_string(_inforbook.Amount) + "|" + to_string(_inforbook.Price) + "|" + _inforbook.Author + "|" + _inforbook.Describe + "|";
	Store_DB("DataBase/Books.txt", _temp);
	bool Check_loadDB_Books = Load_DB_Books("DataBase/Books.txt");
	return true;
}

bool Load_List_of_Borrowed(int &x) // x la so cuon sach da muon
{
	string temp = "";
	string str[10];
	int i = 0;
	int j = 0;
	int count = 0;
	string ss[10];

	// moi file load du lieu len
	ifstream Load_file("DataBase/Borrowed_Book.txt", ios::in);
	if (!Load_file)
	{
		return false;
	}
	while (!Load_file.eof())
	{
		Infor_User_Borrowed clone;
		temp = "";
		getline(Load_file, temp);
		istringstream istr(temp);
		i = 0;
		while (getline(istr, str[i++], '|')){}

		clone.Id_people = str[0];
		clone.Id_Book = str[1];
		clone.Name_Book = str[2];

		istringstream temp1(str[3]);
		j = 0;
		while (getline(temp1, ss[j++], '/')){}
		clone.Date_From.Day = stoi(ss[0]);
		clone.Date_From.Month = stoi(ss[1]);
		clone.Date_From.Year = stoi(ss[2]);

		istringstream temp2(str[4]);
		j = 0;
		while (getline(temp2, ss[j++], '/')){}
		clone.Date_To.Day = stoi(ss[0]);
		clone.Date_To.Month = stoi(ss[1]);
		clone.Date_To.Year = stoi(ss[2]);

		clone.Note = str[5];
		List_Book_Borrowed[count++] = clone;
	}
	x = count;
	Load_file.close();
	return true;
}

int Delete_Book(string Id)
{
	string Name = "";
	int Amount = 0;
	bool check_exits = Check_Id_Book_Exist(Id, Name, Amount);
	if (!check_exits)
	{
		return 0;// Tuc la deo tim thay
	}
	int Number_Book_Borrowed = 0;
	bool check = Load_List_of_Borrowed(Number_Book_Borrowed);
	for (int i = 0; i < Number_Book_Borrowed; i++)
	{
		if (Id.compare(List_Book_Borrowed[i].Id_Book) == 0)
		{
			return 1;
		}
	}
	// Khong ton tai trong ds List_Book_Borrowed thi xoa thoi
	List_Infor_Books.erase(Name);
	bool check_save = Save_DB_Book();
	bool Check_loadDB_Books = Load_DB_Books("DataBase/Books.txt");
	return 2;
}

int Update_Book(string Id)
{
	string Name = "";
	int Amount = 0;
	bool check_exits = Check_Id_Book_Exist(Id, Name, Amount);
	if (!check_exits)
	{
		return 0;// Tuc la deo tim thay
	}
	Infor_Book _temp_update;
	_temp_update.Name = Name;
	_temp_update.Id = Id;
	cout << "So luong: ";
	cin >> _temp_update.Amount;
	cin.ignore();
	cout << "Gia tien: ";
	cin >> _temp_update.Price;
	cin.ignore();
	cout << "Ten tac gia: ";
	getline(cin, _temp_update.Author);
	cout << "Mo ta: ";
	getline(cin, _temp_update.Describe);
	List_Infor_Books[Name] = _temp_update;
	bool check_save = Save_DB_Book();
	bool Check_loadDB_Books = Load_DB_Books("DataBase/Books.txt");
	return 1;
}



//bool Load_List_of_Borrowed_Books_Account_Currently(int &x) // x la so cuon sach da muon
//{
//	x = 0;
//	for (int i = 0; i < Book_Borrowed; i++)
//	{
//		if (List_Book_Borrowed[i].Id_people == Account_Curren.Id)
//		{
//			List_Book_Borrowed_Acc_Currently[x++] = List_Book_Borrowed[i];
//		}
//	}
//	x;
//	return true;
//}

