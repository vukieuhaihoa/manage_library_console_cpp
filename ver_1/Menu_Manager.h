#ifndef MENU_MANAGER
#define MENU_MANAGER

#include "Librarys.h"

void Menu_Manager();
void Load_form_Manager(); //Done
bool Search_Book_Name(string); //Done
bool Accept_Borrow_Book(string , string , string , Date , Date , string); //Coding
bool Check_Id_Book_Exist(string IdBook, string &NameBook, int &Amount);
bool Check_User_Exist(string); //Done
bool Save_DB_Book(); //Done
bool Post_Announce(string, Date); //Done
bool Add_Book(Infor_Book);
int Delete_Book(string Id);
int Update_Book(string Id);
bool Load_List_of_Borrowed(int &x); //Coding
#endif // !MENU_MANAGER
